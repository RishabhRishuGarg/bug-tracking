function GetLocalStorage(TableName) 
{
	return window.localStorage.getItem(TableName);
}
function ValidateSeverity(SeverityName)
{
	var SeverityOptions = ["Blocker","Critical","High","Normal","Enhancement",];
	var SeverityInput = $('[name='+SeverityName+']').val();
	function CheckSeverity (Options) 
	{
		return Options == SeverityInput;
	}
	if ( undefined == SeverityOptions.find(CheckSeverity))
	{
		return false;
	}
	else
	{
		return true;
	}
}
function SetIntoLocalStorage(TableName,Data) 
{
	window.localStorage.setItem(TableName,Data);
}
function CheckLength(Name,Length) 
{
	if( Name.length > Length )
	{
		return false;
	}
	return true;
}
function ConvertIntoJSON(Data) 
{

	return JSON.parse(Data);
}
function ConvertIntoString(Data) 
{
	return JSON.stringify(Data);
}
function ValidateProgrammers(ProgrammersName) 
{
	var ProgrammersList = ConvertIntoJSON(GetLocalStorage('Programmers'));
	for (var i = 0; i < ProgrammersList.length; i++) 
	{

		console.log($('#'+ProgrammersName).val());
		if (ProgrammersList[i].name == $('#'+ProgrammersName).val())
		{
			return true;
		}
	}
	return false;
}
function ValidateInputs(SeverityName,TitleName,DescriptionName,ReportedByName,ProgrammersName) 
{
	if ( !ValidateSeverity(SeverityName) )
	{
		alert("Invalid Severity value");
		return false;
	}

	// Checking for length of title 
	if ( !CheckLength( $('[name='+TitleName +']').val()) )
	{
		alert("Title length crosses the limit. Please enter less character");
		return false;
	}

	// Checking for length of Description 
	if ( !CheckLength($('[name='+DescriptionName+']').val()) )
	{
		alert("Description length crosses the limit. Please enter less character");
		return false;
	}
	ReporterList = ConvertIntoJSON(GetLocalStorage('Reporter'));
	console.log($('[id = '+ ReportedByName+']').val());
	if ( !ReporterList[0].name == $('[id = '+ ReportedByName+']').val() )
	{
		alert("Invalid Reporter");
		return false;
	}
	
	if ( !ValidateProgrammers(ProgrammersName) )
	{	
		alert("Invalid Programmers name entered");
		return false;
	}
	return true;
}
function SetProgrammerAndReporterList() 
{
	
	var ProgrammerList = [
		{name:"Alexandra"},
		{name:"Alice"},
		{name:"Alyssa"},
		{name:"Amber"},
		{name:"Amelia"},
		{name:"Deborah"},
		{name:"Elise"},
		{name:"Finley"},
		{name:"Hannah"},
		{name:"Holly"},
		{name:"Ida"}
	];

	var ReporterList = [
		{ name:"Lauren"}
	];

	ProgrammerList = ConvertIntoString(ProgrammerList);
	ReporterList = ConvertIntoString(ReporterList);

	SetIntoLocalStorage("Programmers",ProgrammerList);
	SetIntoLocalStorage("Reporter",ReporterList);
}
function ShowFormListOnHTML() 
{
	var ProgrammerList = ConvertIntoJSON(GetLocalStorage("Programmers"));
	var ReporterList = ConvertIntoJSON(GetLocalStorage("Reporter"));
	var ProgrammerListHTML = "";
	for (var i = 0; i < ProgrammerList.length; i++) 
	{
		
		ProgrammerListHTML = ProgrammerListHTML +'<option selected="" id="'+ProgrammerList[i].name+'"value="'+ ProgrammerList[i].name+'">'+ProgrammerList[i].name+'</option>';	
	}
	$("#ProgrammerName").append(ProgrammerListHTML);
	$("#ReportedBy").append('<option value="'+ReporterList[0].name+'">'+ReporterList[0].name+'</option>');
	$("#EditProgrammerName").append(ProgrammerListHTML);
	$("#EditReportedBy").append('<option value="'+ReporterList[0].name+'">'+ReporterList[0].name+'</option>');
}
function ShowBugsOnHTML() 
{
	var BugListString = GetLocalStorage("Bugs");
	var BugList = [];
	if(!BugListString || 0 == BugListString.length)
	{
		BugList = [];
	}
	else
	{
		BugList = ConvertIntoJSON(BugListString);
	}
	var BugDataHtml = "";
	for (var i = 0; i < BugList.length; i++) 
	{
		BugDataHtml = BugDataHtml + "<tr>";
		BugDataHtml = BugDataHtml +"<td>"+ BugList[i].Date + "</td>";
		BugDataHtml = BugDataHtml +"<td>"+ BugList[i].Severity + "</td>";
		BugDataHtml = BugDataHtml +"<td>"+ BugList[i].Title + "</td>";
		BugDataHtml = BugDataHtml +"<td>"+ BugList[i].Description + "</td>";
		BugDataHtml = BugDataHtml +"<td>"+ BugList[i].ReportedBy + "</td>";
		BugDataHtml = BugDataHtml +"<td>"+ BugList[i].ProgrammerName + "</td>";	
		BugDataHtml = BugDataHtml + "<td><button type='button' class='btn btn-primary Edit BugShowButton'"
		+ "data-toggle='modal' data-target='#EditBugPopup'  data-Id="+ BugList[i].Id +">Edit</button></td>";
		BugDataHtml = BugDataHtml + "</tr>";
	}
	$("#Bugs").append(BugDataHtml);

}

function SetEditForm(BugId)
{
	console.log(BugId);
	var BugListString = GetLocalStorage("Bugs");
	var BugList = [];
	if(!BugListString || 0 == BugListString.length)
	{
		BugList = [];
	}
	else
	{
		BugList = ConvertIntoJSON(BugListString);
	}
	var SelectedData = {};
	for (var i = 0; i < BugList.length; i++) 
	{
		if ( BugList[i].Id == BugId )
		{

			SelectedData = BugList[i];
			break;
		}
	}
	$('[name=EditId]').attr("value",SelectedData.Id);
	$('[name=EditDate]').attr("value",SelectedData.Date);
	$('[name=EditSeverity]').attr("value",SelectedData.Severity);
	$('[name=EditTitle]').text(SelectedData.Title);
	$('[name=EditDescription]').text(SelectedData.Description);
	$('#'+SelectedData.ReportedBy).attr("selected","selected");
	$('#EditReportedBy').val(SelectedData.ReportedBy);
	$('#EditProgrammerName').val(SelectedData.ProgrammerName);
}

function EditBug()
{
		
	var BugListString = GetLocalStorage("Bugs");
	var BugList = [];
	if(!BugListString || 0 == BugListString.length)
	{
		BugList = [];
	}
	else
	{
		BugList = ConvertIntoJSON(BugListString);
	}
	var SelectedIndex = 0;
	for (var i = 0; i < BugList.length; i++) 
	{
		if ( BugList[i].Id == $('[name=EditId]').attr("value"))
		{
			SelectedIndex = i;
			break;
		}	
 	}
	BugList[SelectedIndex].Date = $('[name="EditDate"]').val();
	BugList[SelectedIndex].Severity = $('[name="EditSeverity"]').val();
	BugList[SelectedIndex].Title = $('[name="EditTitle"]').val();
	BugList[SelectedIndex].Description = $('[name="EditDescription"]').val();
	BugList[SelectedIndex].ReportedBy = $('#EditReportedBy').val();
	BugList[SelectedIndex].ProgrammerName = $('#EditProgrammerName').val();
	SetIntoLocalStorage("Bugs",ConvertIntoString(BugList));
}
function ValidateAndEdit()
{
	if( !ValidateInputs(
			"EditSeverity",
			"EditTitle",
			"EditDescription",
			"EditReportedBy",
			"EditProgrammerName"
		)
	)
	{
		return false;
	}
	EditBug();

}
function GetNextId()
{
	var LastId = GetLocalStorage("LastId");
	LastId = parseInt(LastId, 10);
	LastId = LastId + 1;
	SetIntoLocalStorage("LastId",LastId);
	return LastId;
}
function StoreInputs()
{

	var Bugdata={
		Id: GetNextId(),
		Date: $('[name="Date"]').val(),
		Severity: $('[name="Severity"]').val(),
		Title: $('[name="Title"]').val(),
		Description: $('[name="Description"]').val(),
		ReportedBy: $('#ReportedBy').val(),
		ProgrammerName: $('#ProgrammerName').val(),
	}
	var BugListString = GetLocalStorage("Bugs");
	var BugList = [];
	if(!BugListString || 0 == BugListString.length)
	{
		BugList = [];
	}
	else
	{
		BugList = ConvertIntoJSON(BugListString);
	}
	BugList.push(Bugdata);
	SetIntoLocalStorage("Bugs",ConvertIntoString(BugList));
}
function ValidateAndStore() 
{
	if( !ValidateInputs(
			"Severity",
			"Title",
			"Description",
			"ReportedBy",
			"ProgrammerName"
		)
	)
	{
		return false;
	}
	StoreInputs();
	alert("Bug Report has been added");
}

$(document).ready(function(){
	$(".Bugs").on("click","button.Edit", 
		function()
		{
			SetEditForm($(this).attr("data-Id")); 	
		}
	);
	SetProgrammerAndReporterList();
	ShowFormListOnHTML();
	ShowBugsOnHTML();
});