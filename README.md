# Bug Tracking

# This Repository consist the implementation of Bug Tracking. Bug tracking system keeps track of reported software bugs in software development projects

	
For Bug Tracking we are considering 6 facts
		
		
		1. Time a bug was reported
		2. Severity of the Bug
		3. The erroneous program behavior, 
		4. Description on how to reproduce the bug; 
		5. Reported By - The  person who reported it  
		6. Owner - any programmers who may be working on fixing it
		
Oprations which are allowed on Fact are

		1. A page to list the bugs
		2. The list to show all info regarding the bug
		3. User is able to add/edit bug details on a popup
		4. All data is stored/retrieved in localstorage
		
Technologies 

		Javascript, HTML, jQuery, Bootstrap.